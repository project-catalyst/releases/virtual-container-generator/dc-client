###################################################
#                                                 #
#            DO NOT MODIFY THIS FILE              #
#  to specify the DC settings, use the .env file  #
#                                                 #
###################################################

import json

from abi import CONTRACT_ABI_RAW

DC_NAME = "DC_NAME_PLACEHOLDER"  # must be only characters or underscores
DEBUG = False

# =================================
#   VCG SETTINGS
# =================================

VCG = {
    "PROTOCOL": "VCG_PROTOCOL_PLACEHOLDER",
    "IP": "VCG_IP_PLACEHOLDER",
    "PORT": "VCG_PORT_PLACEHOLDER",
    "PATH": "VCG_PATH_PLACEHOLDER"
}

# =================================
#   ETHEREUM SETTINGS
# =================================

# Wallet address (account) of the DC
WALLET_ADDRESS = "WALLET_ADDRESS_PLACEHOLDER"

# Wallet password. KEEP THIS SECRET!
WALLET_PASSWORD = "WALLET_PASSWORD_PLACEHOLDER"

# Ethereum node host
ETHEREUM_HOST = "localhost"

# Ethereum node port
ETHEREUM_PORT = 8545

# Address of the deployed smart contract
CONTRACT_ADDRESS = "CONTRACT_ADDRESS_PLACEHOLDER"

# ABI of the deployed contract
CONTRACT_ABI = json.loads(CONTRACT_ABI_RAW)