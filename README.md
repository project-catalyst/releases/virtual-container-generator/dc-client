# Catalyst VCG API
This repository holds the VCG API client implementation.

Each Datacenter has its own set of **public-private** keys and a running ethereum node. The transactions are relayed through the VCG API to the corresponding datacenter client.

The connection is established with websockets.

### Required software
1. Docker [engine](https://docs.docker.com/install/linux/docker-ce/ubuntu/).
2. Docker [compose](https://docs.docker.com/compose/install/).

## Prerequisites
1. The **PoA** ethereum network needs to be present and running via ```docker-compose```. The repository and install instructions will be uploaded soon.
2. The **VCG smart contract(s)** must reside inside the blockchain. You can deploy the smart contracts remotely by following the steps in the smart contracts [repo](https://gitlab.com/project-catalyst/VCG/contracts).
3. The **VCG API server** must be present and running. The repository and install instructions can be found [here](https://gitlab.com/project-catalyst/VCG/api-server).


## Deployment

To deploy to a new datacenter, follow these steps:

1. Clone or download the repository.
2. Create a new ethereum account via:

    ```bash
    geth account new --datadir account
    ```
3. Copy the account address (prefix the address generated from the previous command with **0x**) and password inside the ``.env`` file, along with the other possible settings.

    **Be sure to set a unique** ``DC_NAME``.
    
4. Notify the VCG API Server admin of your ``DC_NAME`` and your ``WALLET_ADDRESS``.

5. Once the API Server administrator has registered you, you can run:

    ```bash
    docker-compose up -d
    ```
    
## Example deployment
Inside ``config/example`` there is a docker-compose blueprint that launches two Datacenters with the following details:

| DC_NAME | Wallet address |
| :-----: | :------: |
| DC1     | 0x10f683d9acc908ca6b7a34726271229b846b0292  |
| DC2     | 0x3cf49a570fd253ad0c9b536e5fa8a19c6021d98f  |