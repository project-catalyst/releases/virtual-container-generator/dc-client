FROM ubuntu:bionic

MAINTAINER Artemis Tomaras <tomaras@synelixis.com>

ARG account

ENV TZ=UTC
ENV DC_NAME=$DC_NAME
ENV BOOTNODE_ID=$BOOTNODE_ID
ENV BOOTNODE_IP=$BOOTNODE_IP
ENV VCG_PROTOCOL=$VCG_PROTOCOL
ENV VCG_IP=$VCG_IP
ENV VCG_PORT=$VCG_PORT
ENV VCG_PATH=$VCG_PATH
ENV WALLET_ADDRESS=$WALLET_ADDRESS
ENV WALLET_PASSWORD=$WALLET_PASSWORD

ENV CONTRACT_ADDRESS=$CONTRACT_ADDRESS

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install common packages
RUN apt-get update
RUN apt-get install -y apt-utils build-essential software-properties-common supervisor ntp git curl vim cron unzip apt-utils acl dos2unix net-tools htop python3-pip

# Install locales
RUN apt-get -y install language-pack-en-base language-pack-el-base

# Install geth & tools
RUN add-apt-repository -y ppa:ethereum/ethereum && \
    apt-get update && \
    apt-get install -y ethereum

RUN mkdir /var/log/geth && \
    mkdir /opt/ethereum && \
    mkdir /opt/vcg_api && \
    mkdir /var/log/vcg_api

COPY ./config/run.sh .
COPY ./config/geth/genesis.json /opt/ethereum
COPY $account /opt/ethereum
COPY . /opt/vcg_api

RUN geth init --datadir /opt/ethereum /opt/ethereum/genesis.json

CMD ["bash", "run.sh"]

EXPOSE 30303