#!/bin/bash

echo "Configuration time: $(date)." >> /root/config.log

# Go to deployment dir
cd /opt/vcg_api

# Setup geth
cp config/geth/geth-supervisor.conf /etc/supervisor/conf.d/geth-supervisor.conf
sed -i "s/bootnodeId/$BOOTNODE_ID/g" /etc/supervisor/conf.d/geth-supervisor.conf
sed -i "s/bootnodeIp/$BOOTNODE_IP/g" /etc/supervisor/conf.d/geth-supervisor.conf

# Setup dc client
cp config/supervisor/vcg-api-supervisor.conf /etc/supervisor/conf.d/vcg-api-supervisor.conf
mv settings.py settings.bak
mv prod.py settings.py
sed -i "s/DC_NAME_PLACEHOLDER/$DC_NAME/g" settings.py
sed -i "s/VCG_PROTOCOL_PLACEHOLDER/$VCG_PROTOCOL/g" settings.py
sed -i "s/VCG_IP_PLACEHOLDER/$VCG_IP/g" settings.py
sed -i "s/VCG_PORT_PLACEHOLDER/$VCG_PORT/g" settings.py
sed -i "s#VCG_PATH_PLACEHOLDER#$VCG_PATH#g" settings.py
sed -i "s/WALLET_ADDRESS_PLACEHOLDER/$WALLET_ADDRESS/g" settings.py
sed -i "s/WALLET_PASSWORD_PLACEHOLDER/$WALLET_PASSWORD/g" settings.py
sed -i "s/CONTRACT_ADDRESS_PLACEHOLDER/$CONTRACT_ADDRESS/g" settings.py
pip3 install -r requirements.txt
service supervisor start

echo "Initialization completed."
tail -f /dev/null  # Necessary in order for the container to not stop