from ethereum import CatalystEthereumClient

c = CatalystEthereumClient()
payload = {'name': 'DC1', 'wallet': '0xaa0ae0c5b652c37ef08f988b945f448ad57488df'}
tx_hash = c.register_datacenter(**payload)