import argparse
import json
import signal
import logging

from ws4py.client.threadedclient import WebSocketClient

import settings
import wsaccel

from ethereum import CatalystEthereumClient

wsaccel.patch_ws4py()

logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


def get_parser():
    parser = argparse.ArgumentParser(description='Launches the datacenter ethereum vcg_api client')

    parser.add_argument(
        '-n',
        '--name',
        nargs='?',
        type=str,
        action='store',
        dest='name',
        default=settings.DC_NAME,
        help='Name of the DC (defaults to settings.DC_NAME)'
    )

    return parser


def sig_handler(sig, frame):
    log.info('Caught signal type: {}'.format(sig))
    ws.close()


class VCGApi(WebSocketClient):
    """VCG client worker.

    This class opens a websocket connection to the VCG server and via that connection,
    it transacts with the ethereum blockchain with the provided DC credentials.
    """
    def __init__(self, *args, **kwargs):
        self.ethereum_client = CatalystEthereumClient()
        super(VCGApi, self).__init__(*args, **kwargs)

    def opened(self):
        log.info('Connection to remote opened: {}'.format(self.host))

    def closed(self, code, reason=None):
        log.info('Connection terminated')

    def received_message(self, message):
        payload = json.loads(message.data)
        _request_id = payload.pop('_request_id')

        try:
            response = self.ethereum_client.handle_message(payload)
            self.send(json.dumps({'response': response, '_request_id': _request_id}))
        except Exception as e:
            log.error('Exception: {}'.format(str(e)))
            self.send(json.dumps({
                'response': str(e),
                '_request_id': _request_id,
                'error': True
            }))


def main():
    global ws
    args = get_parser().parse_args()
    vcg_endpoint = "{}://{}:{}/{}{}/".format(
        settings.VCG['PROTOCOL'], settings.VCG['IP'], settings.VCG['PORT'], settings.VCG['PATH'], args.name
    )

    ws = VCGApi(vcg_endpoint, headers=[('wallet', settings.WALLET_ADDRESS)])

    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    try:
        ws.connect()
        ws.run_forever()
    except KeyboardInterrupt:
        ws.close()
    except Exception as e:
        log.error('Exception while running event loop. Reason: {}'.format(str(e)))


if __name__ == '__main__':
    main()
