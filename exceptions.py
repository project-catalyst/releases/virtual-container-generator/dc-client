class InvalidMessageType(Exception):
    pass

class AccountLocked(Exception):
    pass