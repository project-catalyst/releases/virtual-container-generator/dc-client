import json

from abi import CONTRACT_ABI_RAW

DC_NAME = "DC1"  # must be only characters or underscores
DEBUG = True

# =================================
#   VCG SETTINGS
# =================================

VCG = {
    "PROTOCOL": "ws",
    "IP": "192.168.1.215",
    "PORT": 5100,
    "PATH": "ws/"
}

# =================================
#   ETHEREUM SETTINGS
# =================================

# Wallet address (account) of the DC
WALLET_ADDRESS = "0x10f683d9acc908ca6b7a34726271229b846b0292"
#WALLET_ADDRESS = "0x7036E434f15bc8B3F3138C58BDc1549F4B11D6F2"

# Wallet password. KEEP THIS SECRET!
WALLET_PASSWORD = "pwddc1"
#WALLET_PASSWORD = "2781990"

# Ethereum node host
ETHEREUM_HOST = "localhost"

# Ethereum node port
ETHEREUM_PORT = 7545
#ETHEREUM_PORT = 8545

# Address of the deployed smart contract
CONTRACT_ADDRESS = "0xcfeb869f69431e42cdb54a4f4f105c19c080a601"
#CONTRACT_ADDRESS = "0x9137ba016fb14d4f14e9552e5942bca80ac1f697"

# ABI of the deployed contract
CONTRACT_ABI = json.loads(CONTRACT_ABI_RAW)